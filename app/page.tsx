import Mycv from "@/components/Mycv";
import Navbar from "@/components/Navbar";
import Image from "next/image";

export default function Home() {
  return (
    <main className="bg-gray-600 flex justify-center 
    items-center flex-col overflow-hidden mx-auto sm:px-10 px-5">
      <div className="max-w-5xl w-full">
      <Navbar/>
      <Mycv/>
      </div>
    </main>
  );
}
