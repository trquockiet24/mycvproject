import React from 'react'

const Education = () => {
  return (
    <div className='my-2'>
       <h1 className='text-blue-950 font-bold uppercase'>Education: </h1>
      <hr className='border-blue-400 border-t-2 rounded my-2'/>
        <div className="flex justify-between my-2">
            <p>Công Thương College</p>
            <p>Aug 2022 - Feb 2025</p>
        </div>
        <p>Information Technology</p>
    </div>
  )
}

export default Education
