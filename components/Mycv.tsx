import React from 'react'
import Image from 'next/image'
import Summary from './Summary'
import Education from './Education'
import Skills from './Skills'
import Certifications from './Certifications'
import Project from './Project'

const Mycv = () => {
  return (
    <div className='flex flex-col mx-16 my-12 bg-white px-10 py-10'>
        <div className="flex">
            <div className="">
            <Image src="/qk.jpg" alt="avatar" width={100} height={100} className='object-contain'/>
        </div>
        <div className="mx-10">
                <h1 className='text-blue-950 font-bold text-2xl'>TRẦN QUỐC KIỆT</h1>
                <h2 className='text-blue-950 font-bold text-xl'>FRONTEND DEVELOPER</h2>
                    <p>
                        <span className='font-bold mr-5'>Address: </span> Q4, TP.Hồ Chí Minh
                    </p>
                    <p>
                        <span className='font-bold mr-5'>Phone: </span> 0373387152
                    </p>
                    <p>
                        <span className='font-bold mr-5'>Email: </span> trquockiet24@gmail.com
                    </p>
        </div>
        </div>
      <Summary/>
      <Education/>
      <Skills/>
      <Certifications/>
      <Project/>
    </div>
  )
}

export default Mycv
