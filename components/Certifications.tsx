import React from 'react'

const Certifications = () => {
  return (
    <div>
      <div className='my-2 space-y-2'>
    <h1 className='text-blue-950 font-bold uppercase'>CERTIFICATIONS: </h1>
    <hr className='border-blue-400 border-t-2 rounded my-2'/>
        <div className="flex justify-between">
            <p>FreeCodeCamp : Responsive Web Design Certification Viblo</p>
            <p>January, 2024</p>
        </div>
        <div className="flex justify-between">
            <p>Learning: CSS Basic</p>
            <p>December, 2023</p>
        </div>
    </div>
    </div>
  )
}

export default Certifications
