import React from 'react'

const Skills = () => {
  return (
    <div className='my-2'>
    <h1 className='text-blue-950 font-bold uppercase'>Skills: </h1>
    <hr className='border-blue-400 border-t-2 rounded my-2'/>
     <ol className='list-disc list-inside'>
        <li>Programming languages: HTML, CSS, JavaScript, Python</li>
        <li>Framework: React, Nodejs, Tailwind </li>
        <li>Database: MySQL, Firebase </li>
        <li>Soft skill: Teamwork, communication, problem solving and flexibility</li>
     </ol>
    </div>
  )
}

export default Skills
