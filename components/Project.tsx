import Link from 'next/link'
import React from 'react'

const Project = () => {
  return (
    <div id='Project'>
        <h1 className='text-blue-950 font-bold uppercase'>Project: </h1>
        <hr className='border-blue-400 border-t-2 rounded my-2'/>
        <div className="space-y-3">
            <h2 className='font-bold text-xl'>E-commerce Website</h2>
            <h2 className='font-bold'>DEMO: <Link className='underline font-medium' href={"https://qk-eshop.web.app"}>https://qk-eshop.web.app</Link> </h2>
            <p>Personal project</p>
            <p>Technologies:</p>
            <ol className='list-disc list-inside'>
                <li>Frontend: React, TailwindCSS</li>
                <li>Database: FireStore Database</li>
                <li>Backend: Firebase</li>
            </ol>
            <p>Descriptions: E-commerce react responsive website with Firebase Authencation to login and register an account, using Firestore Database to display product, add to cart, make an order. there are 2 types of users Administrator and user. Admin can be see order detail, add product, update product, delete product with a dashboard.</p>
        </div>

        <div className="space-y-3 mt-4">
        <h2 className='font-bold text-xl'>React Chat App</h2>
            <h2 className='font-bold'>DEMO: <Link className='underline font-medium' href={"https://qk-eshop.web.app"}>https://my-appchat-aae83.web.app/</Link> </h2>
            <p>Self-taught project</p>
            <p>Technologies:</p>
            <ol className='list-disc list-inside'>
                <li>Frontend: React, CSS</li>
                <li>Database: FireStore Database</li>
                <li>Backend: Firebase</li>
            </ol>
            <p>Descriptions: Realtime chat application with Firebase Authencation to login and register an account, using Firestore Database to store user detail, chats and message, also its able to upload images by using Firebase Storage then the image will store in the database.</p>
        </div>
    </div>
  )
}

export default Project
