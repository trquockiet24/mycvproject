import Link from 'next/link'
import React from 'react'

const Navbar = () => {
  return (
    <nav className='bg-gray-950 text-white p-4 sm:p-6 md:flex md:justify-between md:items-center'>
        <div className="container mx-auto flex justify-between items-center">
            <a href="/" className='text-3xl font-bold'>
            MyCV
            </a>
            
        </div>
    </nav>
  )
}

export default Navbar
