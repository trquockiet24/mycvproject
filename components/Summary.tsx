import React from 'react'

const Summary = () => {
  return (
    <div className='my-5 Summary'>
      <h1 className='text-blue-950 font-bold uppercase'>Summary: </h1>
      <hr className='border-blue-400 border-t-2 rounded my-2'/>
        <p>Second-year Infomation Techology student at Công Thương College , 
        seeking intership opportunities to apply the knowledge I have learned on papers to practice. My background includes some relevant experience which explains why I
         belive I am suitable for the position of your company.
         </p>
    </div>
  )
}

export default Summary
